import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;



class GameTest {
/// id = 0-7 -CLUBS; 8-15 - DIAMONDS; 16-23 -HEARTS;  24-31 - SPADES
	@Test
	void Compare_GivenTwoSameSuitCardsWhenRulesNonTrump_ReturnsHighestCardOnTrumpRules() {
		Assert.assertEquals(Game.compare(3,4,Game.offer.NON,Game.suit.SPADES), 3);
	}
	@Test
	void Compare_GivenTwoSameSuitCardsWhenRulesAllTrump_ReturnsHighestCardOnNonTrumpRules() {
		Assert.assertEquals(Game.compare(3,4,Game.offer.ALL,Game.suit.SPADES), 4);
	}
	@Test
	void Compare_GivenTwoDifferentSuitCardsWhenRulesALLAndNoneAreTrump_ReturnsCardOfSuitAsked() {
	
		Assert.assertEquals(Game.compare(16,24,Game.offer.ALL,Game.suit.HEARTS),16);
	}
	@Test
	void Compare_GivenTwoSameSuitCardsWhenRulesSuitAsked_ReturnsCardOfSuitAsked() {
		Assert.assertEquals(Game.compare(16,24,Game.offer.SPADES,Game.suit.HEARTS),24);
	}
	@Test
	void Compare_GivenTwoSameSuitCardsWhenRulesDifferentSuit_ReturnsCardOfSuitAsked() {
		Assert.assertEquals(Game.compare(16,24,Game.offer.DIAMONDS,Game.suit.HEARTS),16);

	}
	
	@Test
	void PlayCard_GivenFirstPlayerAllTrumpRules_ReturnsFirstPlayableCard() {
		Game.playerHands = new int[][]{{-1,4,3,-1,-1,-1,7,-1},{-1,-1,17,0,-1,-1,20,-1},{-1,-1,31,23,-1,-1,-1,25},{5,2,28,-1,-1,-1,-1,-1}};
		Game.offer rules = Game.offer.ALL;
		Game.suit suitAsked = Game.suit.SPADES;
		boolean isFirst = true;
		int id = Game.playCard(0, rules, suitAsked, 31, true);
		Assert.assertEquals(4,id);
	}
	@Test
	void PlayCard_GivenSecondPlayerAllTrumpRulesAndHasCardAsked_ReturnsTheHighestPlayableCard() {
		Game.playerHands = new int[][]{ {-1,3,4,-1,-1,-1,7,-1},
										{-1,-1,17,0,-1,-1,0,1},
										{-1,-1,31,23,-1,-1,-1,25},
										{5,2,28,-1,-1,-1,-1,-1}};
		Game.offer rules = Game.offer.ALL;
		Game.suit suitAsked = Game.suit.CLUBS;
		int playerIndex = 1;
		int HighestCardId = 3;
		int id = Game.playCard(playerIndex, rules, suitAsked, HighestCardId, false);
		Assert.assertEquals(1,id);
	}
	@Test
	void PlayCard_GivenSecondPlayerAllTrumpRulesAndHasNoCard_ReturnsTheFirstPlayableCard() {
		Game.playerHands = new int[][]{ {-1,3,4,-1,-1,-1,7,-1},
										{-1,-1,17,15,-1,-1,-1,20},
										{-1,-1,31,23,-1,-1,-1,25},
										{5,2,28,-1,-1,-1,-1,-1}};
		Game.offer rules = Game.offer.ALL;
		Game.suit suitAsked = Game.suit.CLUBS;
		int playerIndex = 1;
		int HighestCardId = 3;
		int id = Game.playCard(playerIndex, rules, suitAsked, HighestCardId, false);
		Assert.assertEquals(17,id);
	}
	@Test
	void PlayCard_GivenSecondPlayerNonTrumpRulesAndHasCardAsked_ReturnsTheHighestPlayableCard() {
		Game.playerHands = new int[][]{ {-1,4,3,-1,-1,-1,7,-1},
										{-1,-1,17,0,-1,-1,0,1},
										{-1,23,-1,-1,-1,31,-1,25},
										{5,2,28,-1,-1,-1,-1,-1}};
		Game.offer rules = Game.offer.NON;
		Game.suit suitAsked = Game.suit.SPADES;
		int playerIndex = 2;
		int HighestCardId = 27;
		int id = Game.playCard(playerIndex, rules, suitAsked, HighestCardId, false);
		Assert.assertEquals(31,id);
	}
	@Test
	void PlayCard_GivenSecondPlayerDiamondRulesAndHasCardAsked_ReturnsTheHighestCardAsked() {
		Game.playerHands = new int[][]{ {-1,4,3,-1,-1,-1,7,-1},
										{-1,-1,17,0,-1,-1,0,2},
										{-1,23,-1,-1,-1,31,-1,25},
										{5,2,28,-1,-1,-1,-1,-1}};
		Game.offer rules = Game.offer.DIAMONDS;
		Game.suit suitAsked = Game.suit.CLUBS;
		int playerIndex = 1;
		int HighestCardId = 7;
		int id = Game.playCard(playerIndex, rules, suitAsked, HighestCardId, false);
		Assert.assertEquals(2,id);
	}
	@Test
	void PlayCard_GivenSecondPlayerDiamondRulesAndHasTrump_ReturnsTheTrumpCard() {
		Game.playerHands = new int[][]{ {-1,4,3,-1,-1,-1,7,-1},
										{-1,-1,17,0,15,-1,0,2},
										{-1,23,-1,-1,-1,31,-1,25},
										{5,2,28,-1,-1,-1,-1,-1}};
		Game.offer rules = Game.offer.DIAMONDS;
		Game.suit suitAsked = Game.suit.SPADES;
		int playerIndex = 1;
		int HighestCardId = 28;
		int id = Game.playCard(playerIndex, rules, suitAsked, HighestCardId, false);
		Assert.assertEquals(15,id);
	}
	@Test
	void PlayCard_GivenSecondPlayerSuitRulesAndHasNoTrump_ReturnsHighestCard() {
		Game.playerHands = new int[][]{ {-1,4,3,-1,-1,-1,7,-1},
										{-1,5,17,0,-1,-1,-1,2},
										{-1,23,-1,-1,-1,31,-1,25},
										{5,2,28,-1,-1,-1,-1,-1}};
		Game.offer rules = Game.offer.DIAMONDS;
		Game.suit suitAsked = Game.suit.SPADES;
		int playerIndex = 1;
		int HighestCardId = 28;
		int id = Game.playCard(playerIndex, rules, suitAsked, HighestCardId, false);
		Assert.assertEquals(5,id);
	}


}
