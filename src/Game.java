import java.util.Stack;

public class Game {
	public int[] deck = new int[32];
	int counter = 0;
	public static int[][] playerHands = new int[4][8];
	int playerCardCount = 0;
	static int[] Trump = {0,1,5,6,3,7,2,4};
	static int[] NoTrump = {0,1,2,4,5,6,3,7};
	public static enum offer {
		CLUBS, DIAMONDS, HEARTS, SPADES, NON, ALL, PASS, CONTRA
	};
	public static enum suit {CLUBS, DIAMONDS, HEARTS, SPADES};
	public void initAndShuffleDeck() {
		for (int i = 0; i < 32; i++) {
			this.deck[i] = i;
		}
		for (int i = 31; i >= 0; i--) {
			int random = (int) (Math.random() *(i+1));
			int temp = deck[i];
			deck[i] = deck[random];
			deck[random] = temp;
		}
	}

	public void dealCards(int numCards) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < numCards; j++) {
				playerHands[i][playerCardCount + j] = deck[counter];
				counter++;
			}
		}
		playerCardCount += numCards;
	}

	public static String printCard(int id) {
		int suit = id / 8;
		int strength = id % 8;
		StringBuilder name = new StringBuilder();
		switch (strength) {
		case 0:
			name.append("7");
			break;
		case 1:
			name.append("8");
			break;
		case 2:
			name.append("9");
			break;
		case 3:
			name.append("10");
			break;
		case 4:
			name.append("J");
			break;
		case 5:
			name.append("Q");
			break;
		case 6:
			name.append("K");
			break;
		case 7:
			name.append("A");
			break;
		}
		switch (suit) {
		case 0:
			name.append((char) '\u2663');
			break;
		case 1:
			name.append((char) '\u2666');
			break;
		case 2:
			name.append((char) '\u2665');
			break;
		case 3:
			name.append((char) '\u2660');
			break;
		default:
			name.append(id);
			break;
		}
		return name.toString();
	}

	public void showCards() {
		for (int i = 0; i < playerCardCount; i++) {
			System.out.println(this.printCard(playerHands[0][i]));
		}
	}


	public static boolean isTrump(int id,offer rules) {
		switch(rules) {
		case CLUBS:
			return id/8==0;
		case DIAMONDS:
			return id/8==1;
		case HEARTS:
			return id/8==2;
		case SPADES:
			return id/8==3;
		case NON:
			return false;
		case ALL:
			return true;
		}
		return false;
	}
	public static int indexOf(int[] array,int number) {
		for(int i=0;i<array.length;i++) {
			if(array[i] == number) return i;
		}
		return -1;
	}
	public static int compare (int id1,int id2,offer rules,suit suitAsked) {
		if(id1/8==id2/8) {
			if(isTrump(id1,rules)) return indexOf(Trump,id1%8)>indexOf(Trump,id2%8)?id1:id2;
			else return indexOf(NoTrump,id1%8)>indexOf(NoTrump,id2%8)?id1:id2;
		}
		else {
				if(suitAsked.ordinal()==rules.ordinal()||rules == offer.ALL)
					return id1/8 == suitAsked.ordinal()?id1:id2;
				else {
					if(id1/8!=suitAsked.ordinal()) return isTrump(id1,rules)?id1:id2;
					return isTrump(id2,rules)?id2:id1;
			}
		}
	} // DONE!
	//counter%4 in switch;
	public static int hasSuitCard(int playerId,suit suitAsked) {
		for(int i=0;i<8;i++) {
			if(playerHands[playerId][i] != -1) {
				if(playerHands[playerId][i]/8 == suitAsked.ordinal())return i;
			}
		}
		return -1;
	}
	public static int hasHigherCard(int playerId,int highestCardId,offer rules,suit suitAsked) { /// returns -1 if not, id in hand array if found
		for(int i=0;i<8;i++) {
			if(playerHands[playerId][i]!=-1&&compare(playerHands[playerId][i],highestCardId,rules,suitAsked)==playerHands[playerId][i]) return i;
		}
		return -1;
	}
	public static int highestCardOfSuit(int playerId,suit suitAsked) {
		int highestCard = -1;
		int highestCardIndex = -1;
		for(int i=0;i<8;i++) {
			if(playerHands[playerId][i]!=-1&&playerHands[playerId][i]/8 == suitAsked.ordinal()&&playerHands[playerId][i]>highestCard) {
				highestCard = playerHands[playerId][i];
				highestCardIndex = i;
			}
		}
		return highestCardIndex;
	}

	public static int playCard(int playerIndex,offer rules,suit suitAsked,int highestCard,boolean isFirst) {
		int cardToPlay = -1;
		if(isFirst) {
			for(int i=0;i<8;i++) {
				if(playerHands[playerIndex][i] != -1) {
					cardToPlay = playerHands[playerIndex][i];
					playerHands[playerIndex][i] = -1;
					return cardToPlay;
				}
			}
		}
		else {
			int suitCard = hasSuitCard(playerIndex,suitAsked);
			if(suitCard!=-1) {
				int cardIndex = highestCardOfSuit(playerIndex,suitAsked);
				cardToPlay = playerHands[playerIndex][cardIndex];
				playerHands[playerIndex][cardIndex] = -1;
				return cardToPlay;
			}
			int higherCardIndex = hasHigherCard(playerIndex,highestCard,rules,suitAsked);
			if( higherCardIndex!= -1) {
				cardToPlay = playerHands[playerIndex][higherCardIndex];
				playerHands[playerIndex][higherCardIndex] = -1;
				return cardToPlay;
			}
			else {
				for(int i=0;i<8;i++) {
					if(playerHands[playerIndex][i] != -1) {
						cardToPlay = playerHands[playerIndex][i];
						playerHands[playerIndex][i] = -1;
						return cardToPlay;
					}
				}
			}
		}
		return cardToPlay;
	}

	public static offer getOffers() {
		offer finalOffer = offer.PASS;
		short passes = 0;
		while (passes < 4) {
			offer current = makeOffer(finalOffer.ordinal());
			if (current != offer.PASS) {
				if (current.ordinal() > finalOffer.ordinal() || finalOffer == offer.PASS) {
					finalOffer = current;
					passes = 0;
				} else
					passes++;
			} else
				passes++;
		}
		return finalOffer;
	}
	//int playCard(int player,offer rules,int firstCard){}
	private int[] playRound(offer rules,int firstPlayer) {
		int[] hand = new int[4] ;
		int[] result = new int[2];
		for(Integer i = 0; i<8;i++) {
			int counter = 0;
			while(counter<4) {
				int nextPlayer = firstPlayer+counter;
				if(nextPlayer>3)nextPlayer-=4;
				//hand[counter] = playCard(firstPlayer+counter);
				counter++;
			}
		}
		return result;
	}
	
	public static offer makeOffer(offer currentOffer,int playerWhoMadeOffer,int currentPlayer) {
		if(playerWhoMadeOffer%2 != currentPlayer%2 ) {
			int counter = 0;
			for(int i=0;i<4;i++) {
				if(playerHands[currentPlayer][i]!=-1&&currentOffer.ordinal()==playerHands[currentPlayer][i]/8) counter++;
				if(counter>3) return offer.CONTRA;
			}
		}
		int call = (int)(Math.random()*101);
		offer result = offer.PASS;
		if(call<=70) return offer.PASS;
		if(call>70&&call<=75) result = offer.CLUBS;
		if(call>75&&call<=80) result = offer.DIAMONDS;
		if(call>81&&call<=85) result = offer.HEARTS;
		if(call>85&&call<=90) result = offer.SPADES;
		if(call>90&&call<=95) result = offer.NON;
		if(call>95) result =  offer.ALL;
		if(currentOffer!=offer.PASS && result.ordinal()<=currentOffer.ordinal())  result = offer.PASS;
		return result;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Game g = new Game();
		g.initAndShuffleDeck();
		g.dealCards(3);
		g.showCards();
		Game.playerHands = new int[][]{ {-1,3,4,-1,-1,-1,7,-1},
			{-1,-1,17,1,0,-1,-1,-1},
			{-1,-1,31,23,-1,-1,-1,25},
			{5,2,28,-1,-1,-1,-1,-1}};
			Game.offer rules = Game.offer.ALL;
			Game.suit suitAsked = Game.suit.SPADES;
			int playerIndex = 1;
			int HighestCardId = 3;
		System.out.println(" --------------");
		System.out.println(Game.suit.SPADES.ordinal());
		System.out.println(hasSuitCard(1, suitAsked));
		System.out.println(-1/8);
	}

}
